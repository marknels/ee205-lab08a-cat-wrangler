///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file queue.hpp
/// @version 1.0
///
/// A generic Queue collection class.
///
/// @author @todo yourName <@todo yourMail@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @todo dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include "node.hpp"
#include "list.hpp"

#pragma once

class Queue {
protected:
	DoubleLinkedList list = DoubleLinkedList();

public:
	inline const bool empty() const { return list.empty(); };

	// Using the above as an example, bring over the following methods from list:
	// Add size()
	// Add push_front()
	// Add pop_back()
	// Add get_first()
	// Add get_next()

}; // class Queue
